pDNS Module Repository
======================

This project is an implementation of a passive DNS replication database (pDNS)
using SQLAlchemy, Alembic and PostgreSQL.

It is geared towards small-ish installations; most likely it is not capable of
handling an ISP's DNS data volume for example.

Learn more `on Read the docs <https://woohoo-pdns.readthedocs.io>`_ (do not
try to read the documentation in the ``docs`` folder).

The source code can be found in `woohoo pDNS' home on Gitlab`_ (link mainly for
people reading this on PyPI).

.. _`woohoo pDNS' home on Gitlab`: https://gitlab.com/scherand/woohoo-pdns
