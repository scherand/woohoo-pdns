.. woohoo pDNS documentation master file, created by
   sphinx-quickstart on Sun Jul 28 23:23:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to woohoo pDNS' documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   usage
   todo
   contribute
   support
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


What woohoo pDNS is
===================

woohoo pDNS is a database to store and query passive DNS data. It is written
in Python 3 and aims to support data collected by the `SiLK NetSA security
suite`_ although it can work with other source data as well.

In addition, it has a `Web GUI component`_ that can be installed separately.

If you want to know in more detail what (a) passive DNS (data(base)) is,
`the FAQ on the Farsight Security website`_ is a valid resource to read up on
the topic.

.. _SiLK NetSA security suite: https://tools.netsa.cert.org
.. _Web GUI component: https://gitlab.com/scherand/woohoo-pdns-gui
.. _the FAQ on the Farsight Security website: https://www.farsightsecurity.com/technical/passive-dns/passive-dns-faq/#q11


What woohoo pDNS is *not*
=========================

woohoo pDNS is *not* optimised for speed. It is geared towards small-ish
installations (read: up to a couple of thoundand inserts per minute?).


Benefits
--------

- Uses SQLAlchemy for the database stuff: let the pros handle SQL
- Adheres to the `Passive DNS - Common Output Format`_.
- Provides a RESTful interface

.. _Passive DNS - Common Output Format: http://tools.ietf.org/html/draft-dulaunoy-dnsop-passive-dns-cof-01
