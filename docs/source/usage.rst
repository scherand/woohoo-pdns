Using woohoo pDNS
=================

There are three ways you can use your installation of woohoo pDNS.

- via the command line interface (CLI)
- via the REST interface
- via the Python API

We well show each of the usage types in turn here.


Command Line Interface (CLI)
----------------------------

The CLI is accessible via the ``pdns`` command. It has a help switch
(``-h/--help``)::

    $ pdns -h
    usage: pdns [-h] [-f CONFIG_FILE] [-q | -v | -d] {load,export,query} ...

    CLI for woohoo pDNS.

    optional arguments:
      -h, --help            show this help message and exit
      -f CONFIG_FILE, --config-file CONFIG_FILE
                            The config file to use, must contain the connection
                            string for the database, e.g. 'conn_str =
                            sqlite:///test.db' in a section called '[DB]'
      -q, --quiet           be quiet (only output critical messages)
      -v, --verbose         be verbose
      -d, --debug           be as verbose as you can

    subcommands:
      available subcommands

      {load,export,query}   see help for respective sub-command
        load                load data into the pDNS database
        export              export data to a JSON file
        query               query the database (returns JSON)

So you could for example load files created by ``supermediator`` using the
following command::

    $ pdns -f pdns.conf load -p "dns.*.txt" /var/spool/silk/dns

The contents of ``pdns.conf`` is described in the Installation guide under
:ref:`install-configfile`.

Or you could query the database as follows::

    $ pdns -f pdns.conf query "*example.com"
    {
        "hitcount": 2,
        "rdata": "example.com",
        "rrname": "example.com",
        "rrtype": 6,
        "time_first": 1479937407.0,
        "time_last": 1479937407.0
    }



RESTful
----------------------------

.. Note::

    You cannot *add* data to the database via the RESTful API.

Once ``gunicorn`` sering the RESTful API (and the reverse proxy protecting it)
is up and running, you can query it using ``curl`` as follows::

    curl -D - -H "Authorization: <your_api_token>" <server_ip>/api/count
    curl -D - -H "Authorization: <your_api_token>" <server_ip>/api/recent
    curl -D - -H "Authorization: <your_api_token>" <server_ip>/api/q/www.example.com


Python API
----------------------------

Here is a sample Python API session::

    import logging
    import configparser
    logging.basicConfig(level=logging.DEBUG)

    from woohoo_pdns.pdns import Database

    config_f = configparser.ConfigParser()
    config_f.read("my.conf")
    conn_str = config_f["DB"]["conn_str"]
    d = Database(conn_str)

    r = d.add_record(1, "foo", "bar.")
    d.find_record(1, "foo")
    d.add_record(1, "foo", "bar", num_hits=40)
    d.find_record(1, "foo")

    d.query("gmail-imap.l.google.com")
    d.query("2a00:1450:4001:080b::200a")
    d.query("127.0.0.1")
    d.query("meteoswiss-app.ch")
    d.query("*example.com")

