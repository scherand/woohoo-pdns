woohoo pDNS' ToDo list or whishlist
===================================


Some things I am considering to add
-----------------------------------

- log some statistics showing the activity in the DB
- implement a Kafka source (:class:`woohoo_pdns.load.Source`)


Some things I am looking for from the community
-----------------------------------------------

- Init scripts (especially for Linux)
- Reverse proxy configurations for other webservers than lighttpd
- General tips and tricks to run Python web applications (e.g. logging)
